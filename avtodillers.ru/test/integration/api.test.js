var vows = require('vows');
var assert = require('assert');
var request = require('supertest');
request = request('http://localhost:8085');

vows.describe('Array')
    .addBatch({
        'request for api': {
            'unnumbered page': {
                topic: function () {
                    request.get('/api').end(this.callback);
                },
                'Api list': function (err, res) {
                    assert.equal(res.statusCode, 200);
                }
            },
            'the page number': {
                topic: function () {
                    request.get('/api/1').end(this.callback);
                },
                'Api list': function (err, res) {
                    assert.equal(res.statusCode, 200);
                }
            },
            'the page error': {
                topic: function () {
                    request.get('/api1').end(this.callback);
                },
                'Api list': function (err, res) {
                    assert.equal(res.statusCode, 404);
                }
            }
        }
    })
    .run();