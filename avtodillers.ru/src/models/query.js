var db = require('../db');
var squel = require("squel");
var paginate = require('simple-pagination');
var countPage = 32;
var level = 7;

function simplePaginate(page, callback) {
    var sql = squel.select()
        .from("query", "q")
        .where("q.source_id = ?", level)
        .where("q.visible = 1")
        .field("COUNT(*)", "total")
        .toString();

    db.query(sql, function (error, result) {
        if (error) throw error;

        var total = result[0].total * 1;
        var pagination = paginate(total, countPage, page);
        var offset = (((pagination.fromCount - 1) > 0)) ? pagination.fromCount - 1 : 0;

        var sql = squel.select()
            .from("query", "q")
            .join("query_image", 'qi', 'qi.query_id = q.id')
            .join("image", "i", 'i.id=qi.image_id')
            .where("q.source_id = ?", level)
            .where("q.visible = 1")
            .where("i.visible = 1")
            .where("i.download = 1")
            .field("q.*")
            .field("i.hash")
            .group("q.id")
            .order("q.id")
            .offset(offset)
            .limit(countPage)
            .toString();

        db.query(sql, function (error, result) {
            if (error) throw error;

            callback(result, pagination);
        });
    });
}

module.exports = {
    "simplePaginate": simplePaginate
};

