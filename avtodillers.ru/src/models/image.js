var db = require('../db');
var squel = require("squel");

function simplePaginate(alias, callback) {

    var sql = squel.select()
        .from("query")
        .where("alias = ?", alias)
        .limit(1)
        .toString();

    db.query(sql, function (error, result) {
        if (error) throw error;

        var query = result[0];

        if (query === undefined) {
            // Not find page
        }

        // список картинок
        var sql = squel.select()
            .field("i.id")
            .field("i.hash")
            .field("i.title")
            .from("image", "i")
            .join("query_image", 'qi', 'qi.image_id = i.id')
            .order("i.id")
            .where("qi.query_id = ?", query.id)
            .where("i.visible = 1")
            .where("i.download = 1")
            .toString();

        db.query(sql, function (error, images) {
            if (error) throw error;

            callback(query, images);
        });
    });
}

function viewImage(id, callback) {
    var sql = squel.select()
        .from("image")
        .where("id = ?", id)
        .where("visible = 1")
        .where("download = 1")
        .toString();

    db.query(sql, function (error, images) {

        if (error) throw error;

        var sql = squel.select()
            .from("query_image")
            .where("image_id = ?", images[0].id)
            .limit(1)
            .toString();

        db.query(sql, function (error, queryImage) {
            if (error) throw error;

            callback(queryImage[0].query_id, images[0]);
        });
    });
}

module.exports = {
    "simplePaginate": simplePaginate,
    "viewImage": viewImage
};