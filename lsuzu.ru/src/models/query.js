var db = require('../db');
var squel = require("squel");
var paginate = require('simple-pagination');
var countPage = 25;
var level = 1;

function simplePaginate(page, callback) {
    var sql = squel.select()
        .from("query")
        .field("COUNT(*)", "total")
        .where("source_id = ?", level)
        .toString();

    db.query(sql, function (error, result) {
        if (error) throw error;

        var total = result[0].total * 1;
        var pagination = paginate(total, countPage, page);
        var offset = (((pagination.fromCount - 1) > 0)) ? pagination.fromCount - 1 : 0;

        var sql = squel.select()
            .from("query")
            .where("source_id = ?", level)
            .order("id")
            .offset(offset)
            .limit(countPage)
            .toString();

        db.query(sql, function (error, result) {
            if (error) throw error;

            callback(result, pagination);
        });
    });
}

module.exports = {
    "simplePaginate": simplePaginate
};

