var imageModel = require('../models/image');

module.exports = {
    index: function (req, res) {
        var alias =  req.params.alias ? req.params.alias.trim() : '';

        imageModel.simplePaginate(alias, function (query, images) {
            res.render('gallery', {"query": query, "list": images});
        });
    },

    view: function (req, res) {
        var imageId = req.params.id ? req.params.id * 1 : 1;

        imageModel.viewImage(imageId, function (query, image) {
            res.render('view', {"image": image});
        });
    }
};