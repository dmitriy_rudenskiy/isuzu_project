var config = require('../../../config.json').mailgun;
var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: config.key, domain: config.domian});

module.exports = {
    send: function (req, res) {

        var refer = req.get('Referrer') || req.headers.referrer || req.headers.referer;

        var name = req.body.name.toString().substr(0, 255).trim();
        var phone = req.body.phone.toString().replace(/[^0-9]/g, '');

        var data = {
            from: 'LandingMailBot <mailgun@' + config.domian + '>',
            to: config.to_mail,
            subject: 'Заявка с сайта',
            text: "Сайт: " + refer + "\n\tЗаявка на просчёт\n\tИмя: " + name + "\n\tТелефон: " + phone
        };

        mailgun.messages().send(data, function (error, body) {
            console.log(body);
        });

        res.send('Ok');
    },
    form: function (req, res) {
        var refer = req.get('Referrer') || req.headers.referrer || req.headers.referer;
        var phone = req.body.phone.toString().replace(/[^0-9]/g, '');

        // Данные из формы
        var sparePart =  req.body.spare_part || '>>>НЕ УКАЗАНО<<<';
        var city = req.body.city || '>>>НЕ УКАЗАНО<<<';
        var maker = req.body.maker || '>>>НЕ УКАЗАНО<<<';
        var model = req.body.model || '>>>НЕ УКАЗАНО<<<';
        var vin = req.body.vin || '>>>НЕ УКАЗАНО<<<';
        var year = req.body.year || '>>>НЕ УКАЗАНО<<<';

        // Text
        var text = "\tНазвание запчасти: " + sparePart + "\n";
        text += "\tВ городе: " + city + "\n";
        text += "\tМарка: " + maker + "\n";
        text += "\tМодель: " + model + "\n";
        text += "\tVin-code или Frame: " + vin + "\n";
        text += "\tГод выпуска: " + year + "\n";

        var data = {
            from: 'LandingMailBot <mailgun@' + config.domian + '>',
            to: config.to_mail,
            subject: '[Новая] Заявка с сайта',
            text: "Сайт: " + refer + "\n\tЗаявка c верхней формы\n\tСодержание: \n\n" + text + "\n\tТелефон: " + phone
        };

        mailgun.messages().send(data, function (error, body) {
            console.log(body);
        });

        res.send('Ok');
    }
};
