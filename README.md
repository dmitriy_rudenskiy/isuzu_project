# Запуск серверов
forever start /var/www/proxy.local/bin/serve.js
forever start /var/www/lsuzu.ru/bin/serve.js
forever start /var/www/isuzy.ru/bin/serve.js
forever start /var/www/izusu.ru/bin/serve.js
forever start /var/www/autodilir.ru/bin/serve.js
forever start /var/www/avtodillers.ru/bin/serve.js
forever start /var/www/dillersauto.ru/bin/serve.js
forever start /var/www/knore-brand.ru/bin/serve.js
forever start /var/www/knorr-bremse.su/bin/serve.js

# Автозапуск серверов
nano /etc/rc.local



# Запускаем с начальными данными
php artisan migrate && php artisan db:seed && php artisan import:query:from:files

# Soft
apt-get install php5-gd

# Add new site
php artisan import:query:from:files
php artisan import:image:from:files
php artisan check:download:images