<?php
namespace App\Console\Commands;

use Gufy\PdfToHtml\Pdf;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class Pdf2html extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:pdf2html';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = 'ekQJG8ioI0EBhB_Z055183-000.pdf';

        $this->info('Start work for file: ' . $name);

        $filePdf = storage_path('convert/in')
            . DIRECTORY_SEPARATOR
            . $name;

        if (!file_exists($filePdf)) {
            $this->error('Not find file');
            return 1;
        }

        $pdf = new Pdf($filePdf);

        $html = $pdf->html();

        $filename = storage_path('convert/out')
            . DIRECTORY_SEPARATOR
            . time() . '.html';

        file_put_contents($filename, $html);
    }
}

