# Запуск сервера

docker run --name work-php --link  work-mysql:db -p 8087:8087  -v '/Users/user/PhpstormProjects/isuzu_project/parser_new:/app' -w '/app' --rm -i -t my/php sh

# Добавляем новый ресурс
INSERT INTO source (id, name) VALUES (10, 'knore-brand.ru');

## Ипортируем новые запросы
php artisan import:query:from:files 10 'knorr_query.csv'
php artisan import:query:from:files 10 'list_query.csv'

!!! Внимание запросы без картинок не отображаются

## Загружаем картинки
php artisan import:image:from:files

mv /var/www/parser_new/storage/image /var/www/knore-brand.ru/public/

docker exec -it some-mysql bash