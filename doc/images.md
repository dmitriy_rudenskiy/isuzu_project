# Создаём папку с картинками
# ln -s файл имя_ссылки
rm -rf '/home/user/PhpstormProjects/isuzu_project/lsuzu.ru/public/image'
ln -s '/home/user/PhpstormProjects/isuzu_project/parser/storage/image' '/home/user/PhpstormProjects/isuzu_project/lsuzu.ru/public/image'

# prod
rm -rf '/var/www/lsuzu.ru/public/image'
ln -s '/var/www/parser/storage/image' '/var/www/lsuzu.ru/public/image'

rm -rf '/var/www/avtodillers.ru/public/image'
ln -s '/var/www/parser/storage/image' '/var/www/avtodillers.ru/public/image'

rm -rf '/var/www/izusu.ru/public/image'
ln -s '/var/www/parser/storage/image' '/var/www/izusu.ru/public/image'