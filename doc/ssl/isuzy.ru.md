# генерируем сертификат
./certbot-auto certonly --manual -d isuzy.ru

# подтверждаем права на домен
mkdir -p /var/www/isuzy.ru/public/.well-known/acme-challenge

echo 'lpGQYjlEWcFdZS1yyP_LSa4nJXFEiy0Hx_b5_hcInB4.qK4ArxdwvBGKj9YFnSfag1bnCEdFLJg6EosqHxAr2Kg' \
> /var/www/isuzy.ru/public/.well-known/acme-challenge/lpGQYjlEWcFdZS1yyP_LSa4nJXFEiy0Hx_b5_hcInB4

# объединяем сетификаты
cat /etc/letsencrypt/live/isuzy.ru/fullchain.pem /etc/letsencrypt/live/isuzy.ru/privkey.pem > /etc/haproxy/cert/isuzy.ru.pem

# проверяем
openssl x509 -noout -text -in /etc/haproxy/cert/isuzy.ru.pem | grep DNS