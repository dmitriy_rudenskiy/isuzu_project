# генерируем сертификат
./certbot-auto certonly --manual -d avtodillers.ru

# подтверждаем права на домен
mkdir -p /var/www/avtodillers.ru/public/.well-known/acme-challenge

echo 'KNjS5TzaGaHIMnMHwoVsB5RkNQxc0SW_5vXr6BZcKok.qK4ArxdwvBGKj9YFnSfag1bnCEdFLJg6EosqHxAr2Kg' \
> /var/www/avtodillers.ru/public/.well-known/acme-challenge/KNjS5TzaGaHIMnMHwoVsB5RkNQxc0SW_5vXr6BZcKok

curl http://avtodillers.ru/.well-known/acme-challenge/KNjS5TzaGaHIMnMHwoVsB5RkNQxc0SW_5vXr6BZcKok

# объединяем сетификаты
cat /etc/letsencrypt/live/avtodillers.ru/fullchain.pem /etc/letsencrypt/live/avtodillers.ru/privkey.pem > /etc/haproxy/cert/avtodillers.ru.pem

# проверяем
openssl x509 -noout -text -in /etc/haproxy/cert/avtodillers.ru.pem | grep DNS