# генерируем сертификат
./certbot-auto certonly --manual -d izusu.ru

# подтверждаем права на домен
mkdir -p /var/www/izusu.ru/public/.well-known/acme-challenge

echo 'hGqpOGvS8AGVMTHT88uYv9rNDQTsY0zPhlxGrl4s7zg.qK4ArxdwvBGKj9YFnSfag1bnCEdFLJg6EosqHxAr2Kg' \
> /var/www/izusu.ru/public/.well-known/acme-challenge/hGqpOGvS8AGVMTHT88uYv9rNDQTsY0zPhlxGrl4s7zg

curl http://izusu.ru/.well-known/acme-challenge/hGqpOGvS8AGVMTHT88uYv9rNDQTsY0zPhlxGrl4s7zg

# объединяем сетификаты
cat /etc/letsencrypt/live/izusu.ru/fullchain.pem /etc/letsencrypt/live/izusu.ru/privkey.pem > /etc/haproxy/cert/izusu.ru.pem

# проверяем
openssl x509 -noout -text -in /etc/haproxy/cert/izusu.ru.pem | grep DNS