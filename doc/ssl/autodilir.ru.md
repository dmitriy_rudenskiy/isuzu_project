# генерируем сертификат
./certbot-auto certonly --manual -d autodilir.ru

# подтверждаем права на домен
mkdir -p /var/www/autodilir.ru/public/.well-known/acme-challenge

echo 'ECuyRxdZH34vqn3UMhp1YuRGLi_RmR-JngtOwXqV3K0.qK4ArxdwvBGKj9YFnSfag1bnCEdFLJg6EosqHxAr2Kg' \
> /var/www/autodilir.ru/public/.well-known/acme-challenge/ECuyRxdZH34vqn3UMhp1YuRGLi_RmR-JngtOwXqV3K0

curl http://autodilir.ru/.well-known/acme-challenge/ECuyRxdZH34vqn3UMhp1YuRGLi_RmR-JngtOwXqV3K0

# объединяем сетификаты
cat /etc/letsencrypt/live/autodilir.ru/fullchain.pem /etc/letsencrypt/live/autodilir.ru/privkey.pem > /etc/haproxy/cert/autodilir.ru.pem

# проверяем
openssl x509 -noout -text -in /etc/haproxy/cert/autodilir.ru.pem | grep DNS