# генерируем сертификат
./certbot-auto certonly --manual -d dillersauto.ru

# подтверждаем права на домен
mkdir -p /var/www/dillersauto.ru/public/.well-known/acme-challenge

echo 'n7PYHzy537idVD7F_sIWMXWfPKvNrmiCvPPcYAlnpVI.qK4ArxdwvBGKj9YFnSfag1bnCEdFLJg6EosqHxAr2Kg' \
> /var/www/dillersauto.ru/public/.well-known/acme-challenge/n7PYHzy537idVD7F_sIWMXWfPKvNrmiCvPPcYAlnpVI

curl http://dillersauto.ru/.well-known/acme-challenge/n7PYHzy537idVD7F_sIWMXWfPKvNrmiCvPPcYAlnpVI

# объединяем сетификаты
cat /etc/letsencrypt/live/dillersauto.ru/fullchain.pem /etc/letsencrypt/live/dillersauto.ru/privkey.pem > /etc/haproxy/cert/dillersauto.ru.pem

# проверяем
openssl x509 -noout -text -in /etc/haproxy/cert/dillersauto.ru.pem | grep DNS