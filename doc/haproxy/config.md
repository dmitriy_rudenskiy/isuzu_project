global
	log /dev/log	local0
	log /dev/log	local1 notice
	chroot /var/lib/haproxy
	stats socket /run/haproxy/admin.sock mode 660 level admin
	stats timeout 30s
	user haproxy
	group haproxy
	daemon

	# Default SSL material locations
	ca-base /etc/ssl/certs
	crt-base /etc/ssl/private

	# Default ciphers to use on SSL-enabled listening sockets.
	# For more information, see ciphers(1SSL). This list is from:
	#  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
	ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS
	ssl-default-bind-options no-sslv3

defaults
	log	global
	mode	http
	option	httplog
	option	dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
	errorfile 400 /etc/haproxy/errors/400.http
	errorfile 403 /etc/haproxy/errors/403.http
	errorfile 408 /etc/haproxy/errors/408.http
	errorfile 500 /etc/haproxy/errors/500.http
	errorfile 502 /etc/haproxy/errors/502.http
	errorfile 503 /etc/haproxy/errors/503.http
	errorfile 504 /etc/haproxy/errors/504.http

frontend http-in
	bind *:80
	bind *:443 ssl crt /etc/haproxy/cert/
    reqadd X-Forwarded-Proto:\ https
    
    redirect scheme https if !{ ssl_fc }

	acl lsuzu hdr(host) -i lsuzu.ru
	acl isuzy hdr(host) -i isuzy.ru
	acl izusu hdr(host) -i izusu.ru
	acl autodilir hdr(host) -i autodilir.ru
	acl avtodillers hdr(host) -i avtodillers.ru
	acl dillersauto hdr(host) -i dillersauto.ru
	
	use_backend https-backend if lsuzu
	use_backend isuzy_backend if isuzy
	use_backend izusu_backend if izusu
	use_backend autodilir_backend if autodilir
	use_backend avtodillers_backend if avtodillers
	use_backend dillersauto_backend if dillersauto

backend lsuzu_backend
	server node1 127.0.0.1:5001

backend isuzy_backend
	server node1 127.0.0.1:5002

backend izusu_backend
	server node1 127.0.0.1:5003

backend autodilir_backend
	server node1 127.0.0.1:7001

backend avtodillers_backend
	server node1 127.0.0.1:7002

backend dillersauto_backend
	server node1 127.0.0.1:7003
