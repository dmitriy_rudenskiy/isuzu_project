var homeController = require('./controllers/home'),
    galleryController = require('./controllers/gallery'),
    pageController = require('./controllers/page'),
    mailController = require('./controllers/mail');

module.exports = function (router) {
    router.get('/', homeController.index);
    router.get('/page/:page', homeController.index);
    router.get('/about', pageController.about);
    router.get('/contact', pageController.contact);
    router.get('/gallery', galleryController.index);
    router.get('/gallery/:alias.html', galleryController.index);
    router.get('/gallery/view/:id', galleryController.view);
    router.post('/mail/send', mailController.send);

    return router;
};