var queryModel = require('../models/query');

module.exports = {
    index: function (req, res) {
        var pageNumber = req.params.page ? req.params.page * 1 : 1;

        queryModel.simplePaginate(pageNumber, function (result, pagination) {
            res.render('home', {"title": "Список", "list": result, "pagination": pagination});
        });
    }
};
