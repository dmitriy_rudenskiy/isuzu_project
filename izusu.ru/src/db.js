var mysql = require('mysql');
var config = require('../../config.json');

var state = {
    pool: null,
    mode: null
};

exports.connect = function (mode, done) {
    state.pool = mysql.createPool(config.mysql);
    state.mode = mode;
    done;
};

exports.get = function () {
    return state.pool
};

module.exports = mysql.createConnection(config.mysql);