var gulp = require('gulp');
var googleWebFonts = require('gulp-google-webfonts');
var concatCss = require('gulp-concat-css');
var concat = require("gulp-concat-js");

gulp.task('fonts', function () {
    return gulp.src('./fonts.list')
        .pipe(googleWebFonts({
            "cssDir": "./../css"
        }))
        .pipe(gulp.dest('./public/fonts'));
});

gulp.task('styles', function () {
    return gulp.src(["./public/css/fonts.css", "./public/css/bootstrap.css"])
        .pipe(concatCss("main.css"))
        .pipe(gulp.dest("./public/css/"));
});

gulp.task('default', ['fonts', 'styles']);