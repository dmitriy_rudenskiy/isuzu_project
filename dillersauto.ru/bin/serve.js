var express = require('express'),
    app = express(),
    path = require('path'),
    config = require('../../config.json'),
    index = 5;

app.use(express.static(path.join(__dirname, './../public')));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.listen(config.sites[index].port, function () {
  console.log('Site ' + config.sites[index].name
      + ' listening on port ' + config.sites[index].port );
});

