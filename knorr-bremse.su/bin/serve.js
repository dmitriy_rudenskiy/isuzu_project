var express = require('express'),
    compression = require('compression'),
    app = express(),
    config = require('../../config.json'),
    index = require('../index.json') * 1,
    path = require('path'),
    bodyParser = require('body-parser'),
    swig = require('swig'),
    db = require( '../src/db');

if (index < 1) {
    throw "Error index site to file index.json";
}

if (typeof(config.sites[index]) === "undefined") {
    throw "Not load config for index: to config.json";
}

// fix
swig.setDefaults({ locals: {
    range: function (start, len) {
        return (new Array(len)).join().split(',').map(function (n, idx) { return idx + start; });
    },
    base_url: (config.env !== 'prod')
        ? 'http://127.0.0.1:' + config.sites[index].port
        :  'http://' + config.sites[index].name
}});

swig.setFilter('image_path', function (hash) {
    return '/' + hash.substr(0, 2) +
            '/' + hash.substr(2, 2) +
            '/' + hash.substr(4, 28) +
            '.jpg'
});

if (config.env !== 'prod') {
    swig.setDefaults({ cache: false });
    app.set('view cache', false);
} else {
    swig.setDefaults({ cache: 'memory' });
}

swig.setDefaults({ cache: false });
app.set('view cache', false);

// compress all requests
app.use(compression());

// config
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, './../public')));

// view engine setup
app.set('views', path.join(__dirname, './../src/views'));
app.engine('twig', swig.renderFile);
app.set('view engine', 'twig');

// routers
app.use(require('./../src/routes')(express.Router()));

// handling 404 errors
app.use(function(req, res, next) {
    if (config.env !== 'prod') {
        console.log("404\t" + req.originalUrl);
    }

    res.status(404).sendFile(path.join(__dirname, './../public/error/404.html'));
});

// Errors
app.use(function(err, req, res, next) {

    if (config.env !== 'prod') {
        console.log(err.stack);
    }

    res.status(err.status || 404);

    if (err.status !== 404) {
        console.log(err.status + "\t" + req.originalUrl);
    }
});

app.listen(config.sites[index].port, function () {
    console.log('Site ' + config.sites[index].name + ' listening on port ' + config.sites[index].port);

    /*
    db.connect(function (error) {
        if (error) throw error;

        db.query('SELECT 1', function (error, results, fields) {
            if (error) throw error;

            console.log('Mysql as id ' + db.threadId);
        });
    });
    */
});

module.exports = app;