var config = require('../../../config.json').mailgun;
var Mailgun = require('mailgun-js');

module.exports = {
    send: function (req, res) {
        var mailgun = new Mailgun({apiKey: config.key, domain: config.domian});
        var refer = req.get('Referrer') || req.headers.referrer || req.headers.referer;

        var name = req.body.name.toString().substr(0, 255).trim();
        var phone = req.body.phone.toString().replace(/[^0-9]/g, '');

        var data = {
            from: 'LandingMailBot <mailgun@' + config.domian + '>',
            to: config.to_mail,
            subject: 'Заявка с сайта',
            text: "Сайт: " + refer + "\n\tЗаявка на просчёт\n\tИмя: " + name + "\n\tТелефон: " + phone
        };

        mailgun.messages().send(data, function (error, body) {
            console.log(body);
        });

        res.send('Ok');
    }
};
